// EX1:Tính tiền lương nhân viên
function tinhTien() {
  var luong1Ngay = document.getElementById("luong1Ngay").value * 1;

  var soNgayLam = document.getElementById("soNgayLam").value * 1;

  var tongTienLuong = luong1Ngay * soNgayLam;

  document.getElementById("tongTienLuong").innerHTML = tongTienLuong;
}

// EX2:Tính giá trị trung bình
function tinhTrungBinh() {
  var soThu1 = document.getElementById("soThu1").value * 1;

  var soThu2 = document.getElementById("soThu2").value * 1;

  var soThu3 = document.getElementById("soThu3").value * 1;

  var soThu4 = document.getElementById("soThu4").value * 1;

  var soThu5 = document.getElementById("soThu5").value * 1;

  var tinhTrungBinh = (soThu1 + soThu2 + soThu3 + soThu4 + soThu5) / 5;

  document.getElementById("tinhTrungBinh").innerHTML = tinhTrungBinh;
}

// EX3: Quy đổi tiền
function quyDoiTien() {
  var soTienNhap = document.getElementById("soTienNhap").value * 1;
  var quyDoiTien = soTienNhap * 23500;
  document.getElementById("quyDoiTien").innerHTML =
    new Intl.NumberFormat("vn-VN").format(quyDoiTien) + "VND";
}

// EX4:Tính chu vi và diện tích hình chữ nhật
function tinhCS() {
  var chieuRong = document.getElementById("chieuRong").value * 1;

  var chieuDai = document.getElementById("chieuDai").value * 1;

  var tinhCV = (chieuDai + chieuRong) * 2;
  var tinhDT = chieuDai * chieuRong;
  document.getElementById("tinhCV").innerHTML = "Chu vi: " + tinhCV;
  document.getElementById("tinhDT").innerHTML = "Diện tích: " + tinhDT;
}

// EX5:Tính tổng 2 ký số
function tinhTong() {
  var nhapSo = document.getElementById("nhapSo").value * 1;

  var hangDonVi = nhapSo % 10;
  var hangChuc = Math.floor(nhapSo / 10);
  var tong = hangChuc + hangDonVi;
  document.getElementById("tinhTong").innerHTML = tong;
}
